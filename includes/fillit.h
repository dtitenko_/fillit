/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 19:34:01 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 18:02:31 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

typedef struct	s_params
{
	int			len;
	int			nb;
}				t_params;

int				get_len_of_grid(int n);
void			ft_move_to_save_forme(int *i, int *j, int len, int pnt);
int				skip_points(char *str, int *i);
void			ft_check_if_can_place(int *i, int *j, int len, char *str);
int				ft_count_points(char *str, int i);
int				ft_check_for(char *grid, char *form, int len, int idx);
int				ft_backtracking(char *grid, char **teriminos,
				t_params size, int j);
void			ft_place_to_grid(char *grid, char *tetri, int len);
int				ft_del_tetri_from_grid(char *grid, char *tetri);
char			*grid_init(char **grid, int len);
void			ft_print_answ(char *grid, int len);
void			start_backtracking(char *grid, char **forms,
				t_params gridparams);

char			*read_file(char	*dest);
char			**split_figures_1(char *input_str);
char			**split_figures_2(char **st_str);
int				ft_valid(char **str);
char			**ft_replace(char *dest);
char			*ft_strtrim1(char const *s, char *ws);
void			print_error();

#endif
