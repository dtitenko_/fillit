# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/28 14:58:16 by dtitenko          #+#    #+#              #
#    Updated: 2016/12/30 18:08:25 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit
CC = gcc
CFLAGS = -Wall -Wextra -Werror

SOURCES_BACKTRACKING =	ft_backtracking.c	ft_backtracking_add.c \
						grid.c				ft_backtracking_add1.c
SOURCES_BACKTRACKING := $(addprefix backtracking/,$(SOURCES_BACKTRACKING))

SOURCES_VALIDATION =	ft_read.c	ft_split.c\
						ft_valid.c	ft_replace.c \
						print_error.c
SOURCES_VALIDATION := $(addprefix validation/,$(SOURCES_VALIDATION))

SOURCES = $(SOURCES_BACKTRACKING) $(SOURCES_VALIDATION) main.c

LIBDIR = ./lib
LIBHEADERS = $(addprefix $(LIBDIR), /includes)
LIBFLAGS = -L$(LIBDIR) -lft


SOURCES_FOLDER = src/



HEADERS = ./includes


OBJECTS_FOLDER = obj/
OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(subst /,__,$(OBJECTS))
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER),$(SOURCES))

all: $(NAME)

.PHONY: all lib re fclean clean

lib:
	@make all -C $(LIBDIR)

$(NAME): lib $(OBJECTS)
	$(CC) $(OBJECTS) $(LIBFLAGS) -o $(NAME)

$(OBJECTS_FOLDER)%.o:
	@mkdir -p $(OBJECTS_FOLDER)
	@pwd
	@$(CC) -I$(LIBHEADERS) -I$(HEADERS) -c $(subst .o,.c,$(subst $(OBJECTS_FOLDER),$(SOURCES_FOLDER),$(subst __,/,$@))) $(CFLAGS) $(MACROS) -o $@
	@printf "$(OK_COLOR)✓ $(NO_COLOR)"
	@echo "$(subst .o,.c,$(subst $(OBJECTS_FOLDER),$(SOURCES_FOLDER),$(subst __,/,$@)))"

clean:
	@make clean -C $(LIBDIR)
	@rm -f $(OBJECTS)
	@rm -rf $(OBJECTS_FOLDER)

fclean: clean
	@rm -f $(NAME)

re: fclean all