/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 19:40:01 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 16:52:48 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	size_t		i;

	if (src == dst)
		return (dst);
	else if (src < dst)
	{
		i = n + 1;
		while (--i)
		{
			*((char *)(dst + i - 1)) = *(char *)(src + i - 1);
		}
	}
	else
	{
		i = -1;
		while (++i < n)
		{
			*((char *)(dst + i)) = *((char *)(src + i));
		}
	}
	return (dst);
}
