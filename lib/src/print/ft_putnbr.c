/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 14:26:05 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/03 15:10:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

void	ft_putnbr(int n)
{
	unsigned int nn;

	if (n < 0)
	{
		ft_putchar('-');
		nn = -n;
	}
	else
		nn = n;
	if (nn > 9)
	{
		ft_putnbr(nn / 10);
		ft_putnbr(nn % 10);
	}
	if (nn < 10)
		ft_putchar('0' + nn);
}
