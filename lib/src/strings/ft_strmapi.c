/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 19:40:18 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 19:48:45 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*fs;
	char	*ss;
	size_t	len;

	if (!s)
		return (NULL);
	len = ft_strlen(s);
	ss = (char *)s;
	if (NULL == (fs = ft_strnew(len)))
		return (NULL);
	while (*ss)
	{
		*fs++ = f(ss - s, *ss);
		ss++;
	}
	return (fs - len);
}
