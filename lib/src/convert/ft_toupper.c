/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 00:33:45 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:46:42 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_islower(int c)
{
	return (('a' <= c && c <= 'z'));
}

int			ft_toupper(int c)
{
	return (ft_islower(c) ? c + 'A' - 'a' : c);
}
