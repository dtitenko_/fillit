/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:11:42 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 18:28:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CONVERT_H
# define FT_CONVERT_H
# include <stdlib.h>

int					ft_toupper(int c);
int					ft_tolower(int c);
int					ft_abs(int nb);
int					ft_atoi(const char *str);
char				*ft_itoa(int n);

#endif
