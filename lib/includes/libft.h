/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:01:04 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 18:20:54 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include "ft_lists.h"
# include "ft_strings.h"
# include "ft_input.h"
# include "ft_memory.h"
# include "ft_print.h"
# include "ft_convert.h"

#endif
