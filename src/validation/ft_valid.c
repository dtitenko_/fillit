/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_valid.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 15:13:57 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 17:53:58 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "fillit.h"

char	**ft_init(void)
{
	char **str_to_compare;

	str_to_compare = (char**)malloc(sizeof(char*) * 19 + 1);
	str_to_compare[0] = ft_strdup("####");
	str_to_compare[1] = ft_strdup("#...#...#...#");
	str_to_compare[2] = ft_strdup("##..#...#");
	str_to_compare[3] = ft_strdup("##...#...#");
	str_to_compare[4] = ft_strdup("#...#...##");
	str_to_compare[5] = ft_strdup("#...#..##");
	str_to_compare[6] = ft_strdup("###...#");
	str_to_compare[7] = ft_strdup("#.###");
	str_to_compare[8] = ft_strdup("###.#");
	str_to_compare[9] = ft_strdup("#...###");
	str_to_compare[10] = ft_strdup("##..##");
	str_to_compare[11] = ft_strdup("##...##");
	str_to_compare[12] = ft_strdup("##.##");
	str_to_compare[13] = ft_strdup("#...##...#");
	str_to_compare[14] = ft_strdup("#..##..#");
	str_to_compare[15] = ft_strdup("#..###");
	str_to_compare[16] = ft_strdup("###..#");
	str_to_compare[17] = ft_strdup("#...##..#");
	str_to_compare[18] = ft_strdup("#..##...#");
	str_to_compare[19] = NULL;
	return (str_to_compare);
}

int		ft_dots_sharps(char **str)
{
	int		i;
	int		j;
	int		dots;
	int		jails;

	i = 0;
	while (str[i])
	{
		dots = 0;
		jails = 0;
		j = 0;
		while (str[i][j])
		{
			if (str[i][j] == '.')
				dots++;
			if (str[i][j] == '#')
				jails++;
			j++;
		}
		if (dots != 12 || jails != 4)
			return (0);
		i++;
	}
	return (1);
}

int		ft_valid(char **str)
{
	char	**str_to_cmp;
	int		i;
	int		j;
	int		check;

	if (ft_dots_sharps(str) == 0)
		return (0);
	str_to_cmp = ft_init();
	i = -1;
	while (str[++i])
	{
		check = 0;
		j = -1;
		while (str_to_cmp[++j])
		{
			if (ft_strstr((const char*)str[i], (const char*)str_to_cmp[j]))
			{
				check = 1;
				break ;
			}
		}
		if (check == 0)
			return (0);
	}
	return (1);
}
