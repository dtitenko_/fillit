/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 15:13:49 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/06 18:13:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "fillit.h"

static int	check_splits(char *str, int splits)
{
	int		i;

	i = 0;
	i = ft_strlen(str);
	if (((i + 1) / 21) == splits && ((i + 1) % 21) == 0)
		return (1);
	return (0);
}

char		**split_figures_1(char *input_str)
{
	int		i;
	char	**b;
	int		spl;

	i = -1;
	spl = 0;
	while (input_str[++i])
		if (input_str[i] == '\n' && input_str[i + 1] == '\n')
		{
			input_str[i + 1] = '*';
			spl++;
		}
	b = ft_strsplit((char const *)input_str, '*');
	if (check_splits(input_str, spl + 1) == 1)
		return (b);
	print_error();
	return (NULL);
}

char		**split_figures_2(char **st_str)
{
	int		i;
	int		j;
	char	**buff;
	char	**b;

	i = 0;
	while (st_str[i])
		i++;
	b = (char**)malloc(sizeof(char*) * i + 1);
	i = -1;
	while (st_str[++i])
	{
		b[i] = (char*)malloc(sizeof(char) * 16 + 1);
		j = -1;
		buff = ft_strsplit((char const *)st_str[i], '\n');
		while (buff[++j])
		{
			if (ft_strlen((const char*)buff[j]) == 4)
				b[i] = ft_strjoin((char const*)b[i], (char const*)buff[j]);
			else
				return (NULL);
		}
	}
	b[i] = NULL;
	return (b);
}
