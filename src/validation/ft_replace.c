/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_replace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 15:14:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/06 18:11:38 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fillit.h"

static int	ft_instr(char ch, char *str)
{
	while (*str != '\0' && *str != ch)
		++str;
	return (*str == ch);
}

char		*ft_strtrim1(char const *s, char *ws)
{
	char	*ends;
	char	*ss;

	if (!s)
		return (NULL);
	while (*s && ft_instr(*s, ws))
		s++;
	if (!*s)
		return ((char *)s);
	ends = (char *)s + ft_strlen(s) - 1;
	while ((char *)s != ends && ft_instr(*ends, ws))
		ends--;
	if (ends - s < 1)
		return (NULL);
	if (!(ss = ft_strnew(ends - s + 1)))
		return (NULL);
	ss = ft_strncpy(ss, s, ends - s + 1);
	return (ss);
}

char		**ft_replace(char *dest)
{
	int		i;
	int		j;
	char	*a;
	char	**b;

	a = read_file(dest);
	b = split_figures_2(split_figures_1(a));
	if (!b || ft_valid(b) == 0)
		print_error();
	i = -1;
	while (b[++i])
	{
		j = -1;
		while (b[i][++j])
		{
			if (b[i][j] == '#')
				b[i][j] = 'A' + i;
		}
		b[i] = ft_strtrim1((const char*)b[i], ".");
	}
	return (i > 26 ? (NULL) : (b));
}
