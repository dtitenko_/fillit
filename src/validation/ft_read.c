/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 15:14:07 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/06 18:12:08 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "libft.h"
#include "fillit.h"

static int	read_amount(char *dest)
{
	int		input_fd;
	int		ret;
	char	buff;

	ret = 0;
	input_fd = open(dest, O_RDONLY);
	while (read(input_fd, &buff, 1))
		ret++;
	if (ret == 0)
		print_error();
	return (ret);
}

static int	check_newlies(char *str)
{
	int		ret;
	int		check;
	int		i;

	ret = 0;
	i = 0;
	check = 0;
	if (str[i] == '.' || str[i] == '#')
		ret = 1;
	i = ft_strlen(str);
	if (i > 2)
		if (str[i - 1] == '\n' && (str[i - 2] == '.' || str[i - 2] == '#'))
			check = 1;
	if (ret == 1 && check == 1)
		return (1);
	return (0);
}

char		*read_file(char *dest)
{
	int		i;
	int		len;
	char	*ret;
	int		input_fd;

	len = 0;
	i = 0;
	if (open(dest, O_RDONLY) == -1)
		print_error();
	if (dest)
	{
		len = read_amount(dest);
		if (len > 0 && (ret = (char*)malloc(sizeof(char) * len + 1)))
		{
			input_fd = open(dest, O_RDONLY);
			while (read(input_fd, &ret[i], 1))
				i++;
			ret[i] = '\0';
			if (check_newlies(ret) == 1)
				return (ret);
		}
	}
	print_error();
	return (NULL);
}
