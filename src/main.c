/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/28 16:56:37 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 17:39:37 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fillit.h"

static void	usage(char *name)
{
	ft_putstr("Usage: ");
	ft_putstr(name);
	ft_putendl(" FILE");
	ft_putendl("Reads tetriminos' from FILE and place them to grid.");
}

static void	work_with_file(char *filepath)
{
	int			i;
	char		**figures;
	char		*grid;
	t_params	params;

	if ((figures = ft_replace(filepath)))
	{
		i = -1;
		while (figures[++i])
			;
		grid = NULL;
		params.nb = i;
		params.len = get_len_of_grid(params.nb - 1);
		grid = grid_init(&grid, params.len);
		start_backtracking(grid, figures, params);
	}
	else
		print_error();
}

int			main(int argc, char **argv)
{
	if (argc == 2)
	{
		if (ft_strequ("--help", argv[1]))
			usage(argv[0]);
		else
			work_with_file(argv[1]);
	}
	else
	{
		ft_putstr(argv[0]);
		ft_putendl(": missing file operand");
		ft_putstr("Try '");
		ft_putstr(argv[0]);
		ft_putendl(" --help' for more information.");
	}
	return (0);
}
