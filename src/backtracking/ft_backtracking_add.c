/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_backtracking_add.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 19:20:54 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 17:24:57 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_check_if_can_place(int *i, int *j, int len, char *str)
{
	int	right_size;
	int	cnt;

	right_size = (len > 3) ? 4 : len;
	if (len >= 5)
	{
		cnt = ft_count_points(str, *i);
		ft_move_to_save_forme(i, j,
			len - right_size + cnt, ft_count_points(str, *i));
	}
	else
	{
		cnt = skip_points(str, i);
		if (len > 1)
			cnt -= 5 - len;
		*j += cnt;
		(*i)--;
	}
}

int		ft_check_for(char *grid, char *form, int len, int idx)
{
	int		i;
	int		j;

	i = ft_count_points(form, 0);
	j = i;
	while (form[i])
	{
		if (form[i] != '.' && form[i + 1] != '.' && form[i + 1]
			&& ((j + idx) % len == len - 1) && (j + idx))
			return (0);
		if ((form[i] == '.' && grid[j] == '.')
			|| (form[i] == '.' && grid[j] != '.'))
			ft_check_if_can_place(&i, &j, len, form);
		else if (form[i] != '.' && grid[j] != '.')
			return (0);
		i++;
		j++;
	}
	return (1);
}

void	ft_place_to_grid(char *grid, char *tetri, int len)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	i = ft_count_points(tetri, 0);
	j = i;
	while (tetri[i])
	{
		if (tetri[i] != '.' && grid[j] == '.')
			grid[j] = tetri[i];
		else if ((tetri[i] == '.' && grid[j] == '.')
			|| (tetri[i] == '.' && grid[j] != '.'))
			ft_check_if_can_place(&i, &j, len, tetri);
		i++;
		j++;
	}
}

int		ft_del_tetri_from_grid(char *grid, char *tetri)
{
	int		i;
	char	c;
	int		pos;

	pos = 0;
	i = 0;
	i = skip_points(tetri, &i);
	c = tetri[i];
	i = -1;
	while (grid[++i])
	{
		if (grid[i] == c && !pos)
			pos = i + 1;
		if (grid[i] >= c && 'A' <= grid[i] && grid[i] <= 'Z')
			grid[i] = '.';
	}
	return (pos);
}
