/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_backtracking_add1.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/30 17:23:43 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 17:23:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_count_points(char *str, int i)
{
	char	*ss;

	ss = (str + i);
	while (*ss == '.')
		ss++;
	return (ss - str - i);
}

void	ft_move_to_save_forme(int *i, int *j, int len, int pnt)
{
	int cnt;

	cnt = 0;
	(*i) += pnt - 1;
	while (++cnt < len)
		(*j)++;
}

int		skip_points(char *str, int *i)
{
	int		cnt;

	cnt = 0;
	(*i)--;
	while ('.' == *(str + (++(*i))))
		cnt++;
	return (cnt);
}
