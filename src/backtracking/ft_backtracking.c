/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_backtracking.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 20:59:51 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 17:21:50 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		start_backtracking(char *grid, char **forms, t_params gridparams)
{
	while (!ft_backtracking(grid, forms, gridparams, 0))
	{
		gridparams.len++;
		grid = grid_init(&grid, gridparams.len);
	}
	ft_print_answ(grid, gridparams.len);
}

int			ft_backtracking(char *grid, char **teriminos,
	t_params gridparams, int j)
{
	int		i;

	if (!gridparams.nb)
		return (1);
	i = -1;
	while (++i < gridparams.len * gridparams.len)
	{
		if ((grid[i] == '.' || teriminos[j][0] == '.')
			&& ft_check_for(&grid[i], teriminos[j], gridparams.len, i))
		{
			ft_place_to_grid(&grid[i], teriminos[j], gridparams.len);
			gridparams.nb--;
			if (!ft_backtracking(grid, teriminos, gridparams, ++j))
			{
				gridparams.nb++;
				ft_del_tetri_from_grid(&grid[i], teriminos[--j]);
			}
			else
				return (1);
		}
	}
	return (0);
}
