/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grid.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 23:33:15 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/30 17:56:42 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fillit.h"

char	*grid_init(char **grid, int len)
{
	ft_strdel(grid);
	if (!(*grid = ft_strnew(len * len)))
		print_error();
	ft_memset(*grid, '.', len * len);
	return (*grid);
}

int		get_len_of_grid(int n)
{
	int		size;
	int		i;

	size = 4 * n + 4;
	i = 0;
	while (i * i < size)
		i++;
	return (i);
}

void	ft_print_answ(char *grid, int len)
{
	int i;

	i = 0;
	while (grid[i])
	{
		ft_putchar(grid[i]);
		i++;
		if (!(i % len) && i)
			ft_putchar('\n');
	}
}
